=== Gravity Forms ACH Field Type ===  
Contributors: mohsinoffline  
Donate link: https://wpgateways.com/contact-us/send-payment/  
Tags: Gravity Forms ACH, ACH, eCheck, payment gateway, Gravity Forms, Gravity Forms payment gateway, payment forms  
Plugin URI: https://wpgateways.com/gravity-forms-ach-echeck-payments/  
Author URI: https://wpgateways.com  
Requires at least: 4.0  
Tested up to: 5.9  
Stable tag: 1.0.2  
License: GNU General Public License v3.0  
License URI: http://www.gnu.org/licenses/gpl-3.0.html 

This plugin enables you to add ACH field type to Gravity Forms.

== Description ==


At WP Gateways, we offer a much needed easy integration ACH (eCheck) payments via our ACH Field Type addon for [Gravity Forms](https://www.gravityforms.com/). This extension adds a new field type called “ACH” and lets you configure it just like the native Credit Card field type and is supported by other WP Gateways add-ons for Gravity Forms including Authorize.Net, USAePay and BluePay.

Not only in the back-end but ACH (eCheck) addon for Gravity Forms adds an equally good looking field on the front end and blends with any WordPress theme without much of a problem.

The ACH (eCheck) addon for Gravity Forms plugin is available for free and is easy to integrate with more payment gateway addons. The list of existing gateway plugins that integrate ACH with Gravity Forms is provided below, and we will continue to make more of our plugins support ACH payments.


**Requirements**
-------
* [Gravity Forms](https://www.gravityforms.com/) plugin 2.3.0 or later, Basic version at the minimum.
* Any compatible payment gateway plugin that supports ACH. (See current list below).

**Extend, Contribute, Integrate**
-------
The plugins currently compatible with the ACH field add-on are (more to be added later):

1. [Authorize.Net Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/authorize-net-gateway-gravity-forms/).
2. [USAePay Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/usaepay-gateway-gravity-forms/).
3. [BluePay Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/bluepay-gateway-gravity-forms/).
4. [NMI Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/network-merchants-nmi-gateway-gravity-forms/).
5. [Cardknox Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/cardknox-gateway-gravity-forms/).
6. [accept.blue Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/accept-blue-gateway-gravity-forms/).
7. [iATS Payments Gateway For Gravity Forms (Advanced Version)](https://wpgateways.com/products/iats-payments-gateway-gravity-forms/).

Visit the [plugin page](https://wpgateways.com/gravity-forms-ach-echeck-payments/) for more details. Contributors are welcome to send pull requests via [Bitbucket repository](https://bitbucket.org/gateways/gf-ach-field).

For custom integration with your WordPress website, please [contact us here](https://wpgateways.com/support/custom-payment-gateway-integration/).

Disclaimer: This plugin is not affiliated with or supported by Gravity Forms. All logos and trademarks are the property of their respective owners.

== Installation ==

1. Upload `gf-ach-field` folder/directory to the `/wp-content/plugins/` directory
2. Activate the plugin (WordPress -> Plugins).
3. Now go to the forms page (WordPress -> Forms) and create/edit a form.
4. Find "ACH" type field under "Pricing Fields" section and setup condition and labels if required.
5. Install a compatible payment gateway plugin (see above for current list).
7. Follow the instructions therein on how to setup feeds that integrate with ACH type field.


== Changelog ==

= 1.0.2 =

* Updated class to make it even more compatible with GF 2.5+
* Updated plugin compatibility list

= 1.0.1 =

* Made eCheck details more descriptive on entry pages

= 1.0.0 =

* Initial release version